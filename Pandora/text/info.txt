BCS Lab Info

When working in these labs, it is important to be aware of the following:

    The BCS Network (a.k.a. the Pandora Network) is completely separate from the Corporate Network.
        This means that your H: Drives in The Corporate Labs are not the same as in the BCS Labs
        None of the resources can be shared (Printers and WIFI)
    The Boppoly Wifi Network
        You will need to login with your corporate credentials
        This wifi network will allow you to access the "normal" internet
    Get Office365 at home
        You will need to login with your corporate credentials
        Office365 for education will give you 5 licenses to use, while you study with us
    Google apps
        You will need to login with your BCS credentials
        (student-id@bcs.net.nz)
        (password you chose when you logged in the first time)
        You will get access to Google Drive (unlimited storage)
    Dreamspark
        You will need to login with your BCS credentials
        (student-id@bcs.net.nz)
        (password you chose for dreamspark)
        This will allow you to download a selection of Microsoft Products and keep for free
